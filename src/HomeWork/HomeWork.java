package HomeWork;

import java.util.ArrayList;

public class HomeWork {
	private String name;
	private ArrayList<Double> scoreList;
	
	public HomeWork(String name){
		this.name = name;
		scoreList = new ArrayList<Double>();
	}
	public double average(){
		double total = 0;
		for(double i:scoreList){
			total+=i;
		}
		return total/scoreList.size();
	}
	public void addScore(double score){
		scoreList.add(score);
	}
	public String toString(){
		return name+" "+average();
	}

}
