package PhoneBook;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class PhoneBookMain{
	public static void main(String[] args){
		String fileName = "phonebook.txt";
		String data = "--------- Java Phone Book ---------\nName Phone\n==== =====\n";
		try {
			FileReader file = new FileReader(fileName);;
		    BufferedReader bf = new BufferedReader(file);
		    String line;
		    while ((line = bf.readLine()) != null) {
		    	String[] ls = line.split(",");
		    	String name = ls[0].trim();
		    	String no = ls[1].trim();
		    	PhoneBook p = new PhoneBook(name,no);
		    	data += p.toString()+"\n";
		    }
			System.out.println(data);
		}
		catch(FileNotFoundException e){
			System.err.println("Can't read file"+fileName);
		}
		catch(IOException e) {
			System.err.println("Error reading file");
		}
	}
}