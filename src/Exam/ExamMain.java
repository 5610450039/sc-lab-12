package Exam;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import HomeWork.HomeWork;

public class ExamMain {
	public static void main(String[] args){
		String fileName = "exam.txt";
		ArrayList<Exam> exam = new ArrayList<Exam>();
		String data = "--------- Exam Scores ---------\nName Average\n==== =====\n";
		
		try{
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bf = new BufferedReader(fileReader);
			FileWriter fileWriter = new FileWriter("average.txt",true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			
			String line;
			while ((line = bf.readLine()) != null) {
				String[] ls = line.split(",");
				for(int i=0;i<ls.length;i++){
					ls[i].trim();
				}
				Exam ex = new Exam(ls[0]);
				for(int i=1;i<ls.length;i++){
					ex.addScore(Double.parseDouble(ls[i]));
				}
				data+=ex.toString()+"\n";
			}
			out.print(data);
			out.close();
			FileReader filePrint = new FileReader("average.txt");
			BufferedReader bufferPrint = new BufferedReader(filePrint);
			String line2;
			for(line2 = bufferPrint.readLine() ; line2 != null ; line2 = bufferPrint.readLine()){
				System.out.println(line2);
			}
			
		}
		catch(FileNotFoundException e){
			System.err.println("Can't read file "+fileName);
		}
		catch(IOException e){
			System.err.println("Error reading file");
		}
	}
}