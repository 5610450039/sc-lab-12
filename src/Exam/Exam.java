package Exam;

import java.util.ArrayList;


public class Exam {
	private String name;
	private ArrayList<Double> scoreList;
	
	public Exam(String name){
		this.name = name;
		scoreList = new ArrayList<Double>();
	}
	
	public double average(){
		double total = 0;
		for(double i:scoreList){
			total+=i;
		}
		return total/scoreList.size();
	}
	public void addScore(double score){
		scoreList.add(score);
	}
	
	public String toString(){
		return name+" "+average();
	}
}